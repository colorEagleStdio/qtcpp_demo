#ifndef LISTENTHREAD_H
#define LISTENTHREAD_H
#include <QHash>
#include <QThread>
#include <QJsonObject>
#include <functional>
#include <fcgi_stdio.h>
class listenThread : public QThread
{
	Q_OBJECT
public:
	explicit listenThread(QObject *parent = 0);
private:
	QHash <QString, std::function< void (const QHash < QString, QString> query_paras, QJsonObject & jsonObj) > >
	m_functions;
	QString m_threadDBName;
protected:
	void run();
	void deal_client(FCGX_Request * request);
	//各个功能函数
	void func_help(const QHash < QString, QString> query_paras, QJsonObject & jsonObj );
	void func_altitude(const QHash < QString, QString> query_paras, QJsonObject & jsonObj );
	void func_object_by_pos(const QHash < QString, QString> query_paras, QJsonObject & jsonObj );
	void func_object_by_name(const QHash < QString, QString> query_paras, QJsonObject & jsonObj );
	void func_geo_by_osmid(const QHash < QString, QString> query_paras, QJsonObject & jsonObj );
};

#endif // LISTENTHREAD_H
