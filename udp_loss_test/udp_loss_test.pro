QT -= gui
QT += network

CONFIG += c++11 console
CONFIG -= app_bundle

win32:{
QMAKE_LIBS += -lws2_32
}
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += \
	udp_loss_test.cpp

HEADERS += \
	udp_loss_test.h

OTHER_FILES += FastUDPRegSetup.reg
