<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DialogFileLoadCtrl</name>
    <message>
        <location filename="dialogfileloadctrl.ui" line="14"/>
        <source>File Load Ctrl</source>
        <translation>文件拷贝与配额控制</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="41"/>
        <source>Watching Folder</source>
        <translation>配额文件夹</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="51"/>
        <location filename="dialogfileloadctrl.ui" line="72"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="62"/>
        <source>Destin Folder</source>
        <translation>目的文件夹</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="83"/>
        <source>Backup files with Size From</source>
        <translation>备份大小介于</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="109"/>
        <source>MB  To</source>
        <translation>MB 到</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="135"/>
        <source>MB</source>
        <translation>MB的文件</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="155"/>
        <source>Total Keep Load</source>
        <translation>配额空间</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="181"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="192"/>
        <source>File Typs(| splitted, : mean empty)</source>
        <translation>备份文件类型(|分割,:表示空扩展名)</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="202"/>
        <source>Running</source>
        <translation>运行开关</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.ui" line="213"/>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="19"/>
        <source>Program Started</source>
        <translation>程序启动</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="35"/>
        <source>Start maintaining.</source>
        <translation>开始维护.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="41"/>
        <source>Completed.</source>
        <translation>完成.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="93"/>
        <source>Fail Deleting %1.</source>
        <translation>删除文件失败:%1.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="194"/>
        <source>Busy File %1 with %2 Bytes.</source>
        <oldsource>Busy File %1 Bytes.</oldsource>
        <translation>文件尚在繁忙: %1 ,%2 字节.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="68"/>
        <source>Force Deleting %1.</source>
        <translation>强制删除文件:%1.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="88"/>
        <source>Succ Deleting %1.</source>
        <translation>删除文件成功:%1.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="159"/>
        <source>Enuming %1 Files %2 Bytes.</source>
        <translation>当前枚举到 %1 个文件，占用 %2 字节.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="192"/>
        <source>Succ Copy %1 to %2.</source>
        <translation>备份文件成功:%1 到 %2.</translation>
    </message>
    <message>
        <source>Fail Copy %1 to %2.</source>
        <translation type="vanished">备份文件失败:%1 到 %2.</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="252"/>
        <source>Watching root</source>
        <translation>配额监视根文件夹</translation>
    </message>
    <message>
        <location filename="dialogfileloadctrl.cpp" line="262"/>
        <source>backup root</source>
        <translation>备份目的根文件夹</translation>
    </message>
</context>
</TS>
