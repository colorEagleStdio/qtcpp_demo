#include <iostream>
#include <atomic>
#include <cstring>
#include "common_example_kafka.h"
using namespace std;


std::atomic<int> run (1);

/**
 * @brief Signal termination of program
 */
void stop(int /*sig*/) {
		run = 0;
}


int main(int argc, char ** argv)
{
	if (strstr(argv[0],"consumer"))
		main_consumer(argc,argv);
	else if (strstr(argv[0],"producer"))
		main_producer(argc,argv);
	else
	{
		printf ("You must raname your exe file to consumer or producer.\n");
		printf ("please choose: 0=exit,1=consumer,2=producer:\n");
		char c = getchar();
		if (c=='1')
			main_consumer(argc,argv);
		else if (c=='2')
			main_producer(argc,argv);

	}
	return 0;
}



/**
 * @returns 1 if all bytes are printable, else 0.
 */
int is_printable(const char *buf, size_t size) {
		size_t i;

		for (i = 0; i < size; i++)
				if (!isprint((int)buf[i]))
						return 0;

		return 1;
}

/**
 * @brief Message delivery report callback.
 *
 * This callback is called exactly once per message, indicating if
 * the message was succesfully delivered
 * (rkmessage->err == RD_KAFKA_RESP_ERR_NO_ERROR) or permanently
 * failed delivery (rkmessage->err != RD_KAFKA_RESP_ERR_NO_ERROR).
 *
 * The callback is triggered from rd_kafka_poll() and executes on
 * the application's thread.
 */
void
dr_msg_cb(rd_kafka_t *rk, const rd_kafka_message_t *rkmessage, void *opaque) {
		if (rkmessage->err)
				fprintf(stderr, "%% Message delivery failed: %s\n",
						rd_kafka_err2str(rkmessage->err));
		else
				fprintf(stderr,
						"%% Message delivered (%zd bytes, "
						"partition %" PRId32 ")\n",
						rkmessage->len, rkmessage->partition);

		/* The rkmessage is destroyed automatically by librdkafka */
}

