#ifndef COMMON_EXAMPLE_KAFKA_H
#define COMMON_EXAMPLE_KAFKA_H
#include <librdkafka/rdkafka.h>
#include <atomic>
extern std::atomic<int> run;
void stop(int /*sig*/);
int is_printable(const char *buf, size_t size);
void dr_msg_cb(rd_kafka_t *rk, const rd_kafka_message_t *rkmessage, void *opaque);
int main_consumer(int argc, char **argv) ;
int main_producer(int argc, char **argv);
#endif // COMMON_EXAMPLE_KAFKA_H
