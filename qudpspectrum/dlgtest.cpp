﻿#include "dlgtest.h"
#include "ui_dlgtest.h"
#include <cmath>
DlgTest::DlgTest(QWidget *parent)
	: QDialog(parent)
	, ui(new Ui::DlgTest)
{
	ui->setupUi(this);
	m_nTid = startTimer(20);
}

DlgTest::~DlgTest()
{
	delete ui;
}

void DlgTest::timerEvent(QTimerEvent * e)
{
	static QUdpSocket sock;
	static qint16 buf[32768];
	static int ctt = 0;
	if (e->timerId()==m_nTid)
	{
		int cp = rand() % 256;
		for (int i = 0;i<2048;++i)
		{
			int cc1 = i /128;
			int cci = i % 128;
			buf[i] = cos(2*3.1415926*i/4.0)*2048+.5;
			buf[i] *= (0.54 - 0.46 * cos (2*3.1415927 * cci / (128)));
			if (((cc1 * 997+cp)%271) %2)
				buf[i] *= -1;
			if (ctt % 100 >= 50)
				buf[i] =0;
			buf[i] += rand() % 32-16;

		}
		++ctt;
		sock.writeDatagram((char *)buf,2048*2,QHostAddress("127.0.0.1"),20338);
	}

}
