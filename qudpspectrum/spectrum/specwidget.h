﻿/* -*- c++ -*- */
/*
 * Gqrx SDR: Software defined radio receiver powered by GNU Radio and Qt
 *           http://gqrx.dk/
 *
 * Copyright 2013 Christian Lindner DL2VCL, Stefano Leucci.
 *
 * Gqrx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * Gqrx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gqrx; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
#ifndef SPECWIDGET_H
#define SPECWIDGET_H

#include <QWidget>
#include <QVariant>
#include <fftw3.h>
#include <vector>
#include <QStandardItemModel>
#include "udp/cudpreciever.h"
namespace Ui {
	class specWidget;
}

class specWidget : public QWidget
{
	Q_OBJECT
	Q_PROPERTY(qint64 sampleRate READ getSampleRate WRITE setSampleRate);

public:
	explicit specWidget(QWidget *parent = nullptr);
	~specWidget();
	void setSampleRate(double rate);
	double getSampleRate(void);
protected:
	void timerEvent(QTimerEvent * e) override;
signals:
	void evt_newCenterFreq(qint64 f);
	void evt_newDemodFreq(qint64 freq, qint64 delta); /* delta is the offset from the center */
	void evt_newLowCutFreq(int f);
	void evt_newHighCutFreq(int f);
	void evt_newFilterFreq(int low, int high);  /* substitute for NewLow / NewHigh */
	void evt_pandapterRangeChanged(double min, double max);
	void evt_newZoomLevel(double level);
	void evt_newCtrlFreq(qint64);
protected slots:
	void slot_new_wav(QByteArray);
	void slot_newDemodFreq(qint64 freq, qint64 delta); /* delta is the offset from the center */
public:
	void appendWavReal(const double * pWav, const int count, double voltage_ref = 16384);
	void appendWavComplex(const double (* pWav)[2], const int count, double voltage_ref = 16384);
public:
	void setNewFftData(const double * rfftData, int size);
	void setNewFftDataWf(const double *  rfftData, const double *  rwfData, int size);
	void setMaxPixmapSize(int w, int h);
	void setRunningState(bool running);
	void setClickResolution(int clickres);
	void setFilterClickResolution(int clickres);
	void setFilterBoxEnabled(bool enabled);
	void setCenterLineEnabled(bool enabled);
	void setTooltipsEnabled(bool enabled);
	void setBookmarksEnabled(bool enabled);
	void setCenterFreq(quint64 f);
	void setFreqUnits(qint32 unit);
	void setDemodCenterFreq(quint64 f);
	void setFilterOffset(qint64 freq_hz);
	qint64 getFilterOffset(void);
	int getFilterBw();
	void setHiLowCutFrequencies(int LowCut, int HiCut);
	void getHiLowCutFrequencies(int *LowCut, int *HiCut);
	void setDemodRanges(int FLowCmin, int FLowCmax, int FHiCmin, int FHiCmax, bool symetric);
	void setSpanFreq(quint32 s);
	void setMaxBandiwdth(int m);
	void setHdivDelta(int delta);
	void setVdivDelta(int delta);
	void setFreqDigits(int digits);
	void setFftCenterFreq(qint64 f);
	int     getNearestPeak(QPoint pt);
	void    setWaterfallSpan(quint64 span_ms);
	quint64 getWfTimeRes(void);
	void    setFftRate(int rate_hz);
	void    clearWaterfall(void);
	bool    saveWaterfall(const QString & filename);
private slots:
	void on_pushButton_reset_clicked();

	void on_doubleSpinBox_center_2_valueChanged(double arg1);

	void on_comboBox_fftSize_activated(int index);

private:
	void loadSettings();
	void saveSettings();
	void resetFFT();
	void freeFFT();
private:
	Ui::specWidget *ui = nullptr;
	QStandardItemModel * m_pDtpMod = nullptr;
private:
	int	m_nFFTSize = 16384;
	cUdpReciever * m_pRevUdp = nullptr;
	std::vector<double> m_dHammingWnd;
	std::vector<double> m_dFFTAmp;
	QByteArray m_arrData;
	fftw_plan m_pFFTPlan = nullptr;
	fftw_complex * m_pFFTIn = nullptr;
	fftw_complex * m_pFFTOut = nullptr;
	int m_nTimerID = -1;
	int m_nCurrentCenter = 0;
	double m_dManuAdd = 0;
};

#endif // SPECWIDGET_H
