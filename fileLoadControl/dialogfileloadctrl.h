#ifndef DIALOGFILELOADCTRL_H
#define DIALOGFILELOADCTRL_H

#include <QDialog>
#include <QFileInfo>
#include <QFileInfoList>
#include <QDir>
#include <QStringList>
#include <map>
#include <QDateTime>
#include <QStandardItemModel>
QT_BEGIN_NAMESPACE
namespace Ui { class DialogFileLoadCtrl; }
QT_END_NAMESPACE

class DialogFileLoadCtrl : public QDialog
{
	Q_OBJECT

public:
	DialogFileLoadCtrl(QWidget *parent = nullptr);
	~DialogFileLoadCtrl();
protected:
	void timerEvent(QTimerEvent * evt) override;
	QFileInfoList enumFiles(QString dirS);
	void updateMap();
	void cleanFile();
	void TransFile(QFileInfo ifile);
private slots:
	void slot_msg(QString);
	void slot_next_prg(int v);
	void on_pushButton_br_src_clicked();
	void on_pushButton_br_dst_clicked();
	void on_checkBox_watch_clicked();

private:
	void loadSettings();
	void saveSettings();
private:
	Ui::DialogFileLoadCtrl *ui = 0;
	QStandardItemModel * m_pMsgMd = 0;
	int m_nTimer = -1;
	std::map<qint64,QMap<QString,QFileInfo> > m_cache_files;
	std::map<qint64,QMap<QString,qint64> > m_cache_sizes;
	qint64 m_total_size = 0;
	qint64 m_total_files = 0;
};
#endif // DIALOGFILELOADCTRL_H
