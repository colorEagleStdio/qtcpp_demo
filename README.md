# C++/Qt知识点测试小程序

本代码库中的小程序和其说明文档的地址如下：

|文件夹|对应文档|
|---|---|
|android/sensors2pc|[使用 Qt for Android 获取并利用手机传感器数据（上篇）开发环境省心搭建](https://goldenhawking.blog.csdn.net/article/details/128137382)|
||[使用 Qt for Android 获取并利用手机传感器数据（下篇）使用C++实现功能](https://goldenhawking.blog.csdn.net/article/details/128161336)|
|chesspi|[一天时间攒一个C++控制台中国象棋程序](https://goldenhawking.blog.csdn.net/article/details/117534567)|
|closure|[C++闭包](https://goldenhawking.blog.csdn.net/article/details/70589476)|
|cpp_rv_ref|[C++右值引用的效果测试](https://goldenhawking.blog.csdn.net/article/details/80588638)|
|cpp_threadpool|[functional助力C++11实现高度可重用接口-线程池例子](https://goldenhawking.blog.csdn.net/article/details/51981905)
|fileLoadControl|[机械盘阵高并发——使用ImDisk 与 junction显著提高整体吞吐性能](https://goldenhawking.blog.csdn.net/article/details/122093588)|
|find0xff|[论坛：二维数组中找最大特征数组](https://goldenhawking.blog.csdn.net/article/details/116240710)|
|findfoo|[大道至简-基于C的库封装发布技术](https://goldenhawking.blog.csdn.net/article/details/119845838)|
|floodfill_mdf|[作业讲评-二值矩阵避障最短路径算法](https://goldenhawking.blog.csdn.net/article/details/109411787)|
|huarongdao|[广度优先求解算法演示（华容道C++代码，速度2644组/秒）](https://goldenhawking.blog.csdn.net/article/details/112414933)
|kafka|[MSYS2显著简化Kafka在windows C++下的使用门槛](https://goldenhawking.blog.csdn.net/article/details/123928247)|
|msys2qtdeployplus|[编写工具调用windeployqt+ldd为msys2 Qt应用程序生成完整发布包](https://goldenhawking.blog.csdn.net/article/details/139725052)|
|nmcalc|[一种基于C++STL库的回溯排列组合枚举器](https://goldenhawking.blog.csdn.net/article/details/80037669)|
|omp_threadpool|[C++与OMP配合的最简线程池](https://goldenhawking.blog.csdn.net/article/details/51824243)|
|pcapio|[UDP丢包替代：用PCAP实现C/C++以太网SDR吞吐](https://goldenhawking.blog.csdn.net/article/details/126292692)|
|primer_calc|[C++ OpenMP有限资源快速连续素数求取](https://goldenhawking.blog.csdn.net/article/details/109043815)|
|profile_log|[为C++/Qt加入轻便性能收集器](https://goldenhawking.blog.csdn.net/article/details/90245867)|
|qflightinstruments|[一款有意思的 Qt 飞行仪表控件](https://goldenhawking.blog.csdn.net/article/details/78817426)|
|qt_coro_test|[使用 C++23 协程实现第一个 co_await 同步风格调用接口--Qt计算文件哈希值](https://goldenhawking.blog.csdn.net/article/details/136227836)|
|qt_event_signal|[Qt Signals & Slots VS QEvents - Qt跨线程异步操作性能测试与选取建议](https://goldenhawking.blog.csdn.net/article/details/133996118)
|qt_fcgi|[用C++Qt 与libfcgi快速开发后台 WebService](https://goldenhawking.blog.csdn.net/article/details/69672429)|
|qudpspectrum|[Qt 三种实时时频控件的简易方案](https://goldenhawking.blog.csdn.net/article/details/104087793)|
|sdr_deepdetect|[从火星传图有多难-业余眼光看深空通信](https://goldenhawking.blog.csdn.net/article/details/117070293)|
|sharedptr_ndarray|[share_ptr在C++多维数组管理方法中的优势和性能测试](https://goldenhawking.blog.csdn.net/article/details/78171742)|
|string_split|[C++11分解字符串](https://goldenhawking.blog.csdn.net/article/details/80833565)|
|tunnel_speed|[基于进程管道的SDR最大吞吐速率测试](https://goldenhawking.blog.csdn.net/article/details/124575558)|
|udp_loss_test|[(Win32) QUdpSocket 丢包测试与解决](https://goldenhawking.blog.csdn.net/article/details/105622237)
|uhd_cpp|[USRP B210 SDR C/C++接口连续收发程序](https://goldenhawking.blog.csdn.net/article/details/109907083)|
||[USRP 套件在windows下的快速引入](https://goldenhawking.blog.csdn.net/article/details/110727815)|
