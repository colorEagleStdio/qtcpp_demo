#ifndef FINDFOO_H
#define FINDFOO_H
#include "findfoo_global.h"
#ifdef __cplusplus
extern "C"{
#endif
FINDFOO_EXPORT FFHANDLE		FOOCALL		ff_init_task	(const char * task);
FINDFOO_EXPORT void			FOOCALL		ff_reset_task	(FFHANDLE h	, const char * task);
FINDFOO_EXPORT const char * FOOCALL		ff_get_task		(FFHANDLE h	);
FINDFOO_EXPORT long long	FOOCALL		ff_find			(FFHANDLE h	, const char * rawStr);
FINDFOO_EXPORT void			FOOCALL		ff_fini_task	(FFHANDLE h	);
#ifdef __cplusplus
}
#endif
#endif // FINDFOO_H
