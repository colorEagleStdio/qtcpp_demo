#include <vector>
#include <array>
#include <functional>
#include <map>
#include <unordered_map>
#include <stdio.h>
#include <time.h>
#include <malloc.h>
#include <memory>
#include <typeinfo>
//GNU C++ 7.2
//链接选项: -Wl,--stack,20240000
using namespace std;
const int testloopTimes = 1000;
const int d1 = 80;
const int d2 = 60;
const int d3 = 40;
const int d4 = 20;
//用于进行循环赋值测试的函数
template <typename T>
double function_loop(T p, int Ntimes)
{
	double  duration = 0;
	clock_t start, finish;
	start=clock();
	for (int c=0;c<Ntimes;++c)
		for(int h=0; h<d1; h++)
			for(int i=0; i<d2; i++)
				for(int j=0; j<d3; j++)
					for(int k=0; k<d4; k++)
						p[h][i][j][k]=h*60+i*20+j*5+k + c;
	finish=clock();
	duration=(double)(finish-start)/CLOCKS_PER_SEC;
	if (Ntimes>1)
		printf("\tloop cost: %f (s)\n",duration);
	return duration;
}
//用于测试含有构造、析构的函数
void test(function<double (void)> ftest)
{
	double  duration = 0;
	clock_t start, finish;
	start=clock();
	double loopCost = ftest();
	finish=clock();
	duration=(double)(finish-start)/CLOCKS_PER_SEC;
	printf("\tMEM  cost: %f (s)\n",duration - loopCost );
}
//vector 测试
double function_vector()
{
	vector<vector<vector<vector<int> > > >
			p(d1,vector<vector<vector<int> > >
			  (d2,vector<vector<int> >
			   (d3,vector<int>(d4,0))));
	printf ("test vector:\n");
	double loopCost = function_loop(p,testloopTimes);
	return loopCost;
}
//array 测试
double function_array()
{
	array<array<array<array<int,d4 >,d3 >,d2 >,d1> p{0};
	printf ("test array:\n");
	double loopCost = function_loop(p,testloopTimes);
	return loopCost;
}
//map 测试
double function_map()
{
	map<int,map<int,map<int,map<int,int > > >> p;
	printf ("test map:\n");
	function_loop(p,1);
	double loopCost = function_loop(p,testloopTimes);
	return loopCost;
}
//unordered_map 测试
double function_unordered_map()
{
	unordered_map<int,unordered_map<int,unordered_map
			<int,unordered_map<int,int > > >> p;
	printf ("test unordered_map:\n");
	function_loop(p,1);
	double loopCost = function_loop(p,testloopTimes);
	return loopCost;
}
//shared_ptr 测试
#if __GNUC__>= 7
double function_shared_ptr()
{
	shared_ptr<int[][d2][d3][d4]> p (new int [d1][d2][d3][d4]);
	printf ("test shared_ptr:\n");
	double loopCost = function_loop(p,testloopTimes);
	return loopCost;
}
#endif

//pointer测试
double function_pointer()
{
	printf ("test pointer:\n");
	int ****p = nullptr;
	int h,i,j;
	p=new int ***[d1];
	if (nullptr==p) return 0;
	for (h=0; h<d1; h++)
	{
		p[h]=new int ** [d2];
		if (nullptr==p[h]) return 0;
		for (i=0; i<d2; i++)
		{
			p[h][i]=new int *[d3];
			if (nullptr==p[h][i]) return 0;
			for (j=0; j<d3; j++)
			{
				p[h][i][j]=new int [d4];
				if (nullptr==p[h][i][j]) return 0;
			}
		}
	}
	double loopCost = function_loop(p,testloopTimes);
	for (h=0; h<d1; h++)
	{
		for (i=0; i<d2; i++)
		{
			for (j=0; j<d3; j++)
			{
				delete [] p[h][i][j];
			}
			delete [] p[h][i];
		}
		delete [] p[h];
	}
	delete [] p;
	return loopCost;
}

int main()
{
	test(function_vector);
	test(function_array);
	test(function_pointer);
	test(function_map);
	test(function_unordered_map);
#if __GNUC__>= 7
	test(function_shared_ptr);
#endif
	return 0;
}
