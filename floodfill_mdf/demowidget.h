﻿#ifndef DEMOWIDGET_H
#define DEMOWIDGET_H

#include <QWidget>
#include <vector>
#include "floodfill_mdf.h"
class demoWidget : public QWidget
{
	Q_OBJECT
public:
	explicit demoWidget(QWidget *parent = nullptr);
protected:
	//0,1联通矩阵，0是障碍，1是可通区域
	std::vector<std::vector<char> > v_mat;
	int iter = 0;
protected:
	void paintEvent(QPaintEvent * evt) override;
	void mousePressEvent(QMouseEvent * evt) override;
};

#endif // DEMOWIDGET_H
