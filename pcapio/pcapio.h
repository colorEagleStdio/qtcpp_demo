#ifndef PCAPIO_H
#define PCAPIO_H
#include <string>
#include <map>

namespace PCAPIO{

std::string pcapio_interfaces(std::map<std::string,std::string> & devmap);


}

#endif // PCAPIO_H
