TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cpp \
    chesspi_rules.cpp \
    chesspi_ai.cpp

HEADERS += \
    chesspi.h

QMAKE_CXXFLAGS += -fopenmp
LIBS += -lgomp
