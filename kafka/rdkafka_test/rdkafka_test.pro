TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
SOURCES += \
	consumer.cpp \
	main.cpp \
	producer.cpp

HEADERS += \
	common_example_kafka.h
win32:LIBS+=-lrdkafka.dll
linux:LIBS+=-lrdkafka
