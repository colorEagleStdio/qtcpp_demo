﻿#ifndef FLOODFILL_MDF_H
#define FLOODFILL_MDF_H
#include <vector>
int min_distance_find(
		const std::vector<std::vector<char> >  & v_mat ,
		const int startx,
		const int starty,
		const int endx,
		const int endy,
		std::vector<int>  *x,//着色点
		std::vector<int>  *y,
		std::vector<int>  *pidx,//关键点
		bool join = true,
		bool safe_join = true,
		int max_itertimers = 16
		);
#endif // FLOODFILL_MDF_H
