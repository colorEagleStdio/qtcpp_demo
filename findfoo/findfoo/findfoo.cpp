#include "findfoo.h"
#include <assert.h>
#include <string>
class Findfoo
{
public:
	Findfoo(const std::string & task = "foo");
	~Findfoo();
public:
	void setTask(const std::string & task);
	const std::string &  task() const;
	long long Find(const std::string & rawStr);
private:
	std::string m_task = "foo";
};


Findfoo::Findfoo(const std::string & task)
	:m_task(task)
{
}
Findfoo::~Findfoo()
{

}

void Findfoo::setTask(const std::string & task)
{
	m_task = task;
}
const std::string &  Findfoo::task() const
{
	return m_task;
}

long long Findfoo::Find(const std::string & rawStr)
{
	return rawStr.find(m_task);
}

//-----------

FINDFOO_EXPORT FFHANDLE FOOCALL ff_init_task(const char * task)
{
	Findfoo * f = new Findfoo(task);
	return (FFHANDLE) f;
}

FINDFOO_EXPORT void FOOCALL ff_reset_task(FFHANDLE h, const char * task)
{
	Findfoo * f = reinterpret_cast<Findfoo *>(h);
	assert(f);
	f->setTask(task);

}

FINDFOO_EXPORT const char * FOOCALL ff_get_task(FFHANDLE h)
{
	Findfoo * f = reinterpret_cast<Findfoo *>(h);
	assert(f);
	return f->task().c_str();
}

FINDFOO_EXPORT long long FOOCALL ff_find(FFHANDLE h, const char * rawStr)
{
	Findfoo * f = reinterpret_cast<Findfoo *>(h);
	assert(f);
	return f->Find(rawStr);
}

FINDFOO_EXPORT void FOOCALL ff_fini_task(FFHANDLE h)
{
	Findfoo * f = reinterpret_cast<Findfoo *>(h);
	if (f)
		delete f;
}
