#ifndef TESTEVENT_H
#define TESTEVENT_H

#include <QEvent>
#include <time.h>
#include <QString>
class TestMsg : public QEvent
{
private:
	static QEvent::Type m_testEvt;
	static QEvent::Type m_startEvt;
	static QEvent::Type m_startSig;
	static QEvent::Type m_quit;
	clock_t m_clk = 0;
	QString m_dummyLongStr;
public:
	TestMsg(clock_t clk);
	~TestMsg();
	inline clock_t clock() {return m_clk;}
	inline void fillStr(int len)
	{
		for (int i=0;i<len;++i)
		{
			m_dummyLongStr.push_back((char)(i*37%64+32));
		}
	}
public:
	static inline QEvent::Type type() {return m_testEvt;}
	static inline QEvent::Type startEvt() {return m_startEvt;}
	static inline QEvent::Type startSig() {return m_startSig;}
	static inline QEvent::Type quitEvt() {return m_quit;}
};

#endif // TESTEVENT_H
