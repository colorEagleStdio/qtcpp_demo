CONFIG -= qt

TEMPLATE = lib
DEFINES += FINDFOO_LIBRARY
DESTDIR = $$OUT_PWD/../bin
CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    findfoo.cpp

HEADERS += \
    findfoo_global.h \
    findfoo.h


