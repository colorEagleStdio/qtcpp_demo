#ifndef DIALOGKAFKA_H
#define DIALOGKAFKA_H

#include <QDialog>
#include <thread>
#include <QStandardItemModel>
#include "kafka_client.h"
QT_BEGIN_NAMESPACE
namespace Ui { class DialogKafka; }
QT_END_NAMESPACE

using namespace KafkaClient;
class DialogKafka : public QDialog
{
	Q_OBJECT

public:
	DialogKafka(QWidget *parent = nullptr);
	~DialogKafka();
	//发送记录。由于可能来自多个线程，用信号周转
	void shootMsg(QString);
protected:
	void timerEvent(QTimerEvent * evt) override;
private slots:
	void on_pushButton_start_clicked();
	//消息槽
	void slot_msg(QString);
private:
	Ui::DialogKafka *ui;
	int m_nTimerID =-1;
	QStandardItemModel * m_pMsgMod = nullptr;
protected:
	kafka_producer * producer = nullptr;
	kafka_consumer * consumer = nullptr;
	std::thread * m_runthread = nullptr;
signals:
	void sig_msg(QString);
};
#endif // DIALOGKAFKA_H
