//main.cpp
#include <QCoreApplication>
#include <QThread>
#include "profile_log.h"
void  foo() 
{
	//一行完成标记
	LOG_PROFILE("FOO deal","Start (100 times)");
#pragma omp parallel for
	for (int i=0;i<100;++i)
	{
		//模拟多线程耗时操作
		QThread::msleep(rand()%20+40);
		LOG_PROFILE("FOO deal",QString("T%1").arg(i));
	}
	LOG_PROFILE("FOO deal","Finished ");
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	//直接初始化，使用UUID的唯一文件名，在当前路径下的log创建。
	profile_log::init();
	//开启日志。在发布时，可以设为false。
	profile_log::set_log_state(true);
	//测试
	foo();
	return 0;
}
