TEMPLATE = app
CONFIG += console c++20
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
	uhd_the_ark.cpp
LIBS += -luhd -lpthread -lgomp -lfftw3
QMAKE_CXXFLAGS_RELEASE += -fopenmp -O3 -fopenmp  -mavx2
