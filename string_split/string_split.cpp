#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <regex>
using namespace std;

int main()
{
    string s{"12,-2.3,-1.2e-3;;1023.283784,.12,-.23;"};
    regex regex{"([^\\d\\.eE\\-])"};
    sregex_token_iterator it{s.begin(), s.end(), regex, -1};
    list<string> words{it, {}};
    vector<double> values;

    for_each(words.begin(),words.end(),[&values](string v){
        values.push_back(atof(v.c_str()));
    });

    for_each(values.begin(),values.end(),[](double v){
        cout<<v<<"\n";
    });

    return 0;
}
