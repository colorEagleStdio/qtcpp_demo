#include "dialogfileloadctrl.h"
#include <QApplication>
#include <QLibraryInfo>
#include <QTranslator>
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QTranslator qtTranslator;
	qtTranslator.load("qt_" + QLocale::system().name(),
					  QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	a.installTranslator(&qtTranslator);

	QTranslator appTranslator;
	QString strTransLocalFile =":/" +
			QCoreApplication::applicationName()+"_"+
			QLocale::system().name()+".qm";
	appTranslator.load(strTransLocalFile );
	a.installTranslator(&appTranslator);


	DialogFileLoadCtrl w;
	w.show();
	return a.exec();
}
