#include "udp_loss_test.h"

/*
Windows 下请直接修改注册表：

Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\AFD\Parameters]
"DefaultReceiveWindow"=dword:00100000
"FastSendDatagramThreshold"=dword:00002800
"DefaultSendWindow"=dword:00100000

另存为 udp.reg,双击导入后重启计算机，即可在Debug模式下，保证QUdpSocket一包不丢

*/

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	//Start Sending
	sendingThread * tsend = new sendingThread;
	tsend->start(QThread::HighestPriority);
	QThread::msleep(3000);
	printf("Start...\n");

	//test UDP loop
	int total_loop = test_Loop();
	printf("QUdpSocket LOOP:\n\tSend %d, Recv %d, Lost %d.\n",
							   total_loop,tests,total_loop-tests );

	QThread::msleep(3000);
	printf("Start...\n");

	//test Class
	QThread * trecv = new QThread;
	TestObj * sobj = new TestObj;
	sobj->moveToThread(trecv);
	trecv->start();
	sobj->start();
	trecv->wait();
	sobj->deleteLater();
	trecv->deleteLater();
	const int totaltest_ss = sobj->endID()-sobj->startID()+1;
	printf("QUdpSocket Signal and Slots:\n\tSend %d, Recv %d, Lost %d.\n",
							   totaltest_ss,
							   tests,
							   totaltest_ss-tests );

	QThread::msleep(3000);
	printf("Start...\n");

	//Test Local
	const int local_total = local_test();
	printf("Local Socket :\n\tSend %d, Recv %d, Lost %d.\n",
							   local_total,
							   tests,
							   local_total-tests );
	//end
	tsend->stop();
	tsend->wait();
	tsend->deleteLater();
	QCoreApplication::processEvents();
	getchar();
	return 0;
}
