TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
	../waveform.cpp \
	main.cpp

LIBS += -luhd

HEADERS += \
	../waveform.h
win32{
    INCLUDEPATH +="$$PWD/../3rdlibs/fftw"
    contains(QT_ARCH, i386) {
	LIBS+=-L"$$PWD/../3rdlibs/fftw/x86" -llibfftw3-3
    } else {
	LIBS+=-L"$$PWD/../3rdlibs/fftw/x64" -llibfftw3-3
    }
    INCLUDEPATH +="$(UHD_PKG_PATH)/include"
    LIBS += -L"$(UHD_PKG_PATH)/lib"
}else {
LIBS+=-lfftw3 -lpthread -lboost_system
}
