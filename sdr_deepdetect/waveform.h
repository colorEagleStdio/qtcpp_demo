#ifndef WAVEFORM_H
#define WAVEFORM_H
typedef short SPTYPE;
#define		MODRATE_N	1
#define		C_AMP		8192
#define		SPREAD_RAT	512
#define		WAVSIZE		SPREAD_RAT*MODRATE_N
#define		SIGRATE		200001
extern SPTYPE wav_spread[2][WAVSIZE][2];
extern float wav_xorr[2][WAVSIZE][2];
void init_wavform();
#endif // WAVEFORM_H
