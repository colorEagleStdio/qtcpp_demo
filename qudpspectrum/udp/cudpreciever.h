#ifndef CUDPRECIEVER_H
#define CUDPRECIEVER_H

#include <QThread>
#include <QUdpSocket>

class cUdpObject :  public QObject
{
	Q_OBJECT
public:
	explicit cUdpObject(QObject *parent = nullptr);
	~cUdpObject() override;
	bool isRunning() const {return m_pSock==nullptr?false:true;}
public slots:
	void bind(int port);
	void unbind();
	void quit();
signals:
	void newSig(QByteArray);
protected slots:
	void readData();
private:
	QUdpSocket * m_pSock = nullptr;
	int m_nPort = 0;
};

class cUdpReciever : public QObject
{
	Q_OBJECT
public:
	explicit cUdpReciever(QObject *parent = nullptr);
	~cUdpReciever() override;
public:
	bool isRunning();
public slots:
	void startListen(int port);
	void stopRecv();
signals:
	void newSig(QByteArray);
	void cmd_start(int port);
	void cmd_stop();
	void cmd_quit();
protected:
	cUdpObject * m_pUdpObj = nullptr;
	QThread * m_pWThread = nullptr;
};

#endif // CUDPRECIEVER_H
