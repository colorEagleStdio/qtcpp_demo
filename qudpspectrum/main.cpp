#include "dlgtest.h"

#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	DlgTest w;
	w.show();
	return a.exec();
}
