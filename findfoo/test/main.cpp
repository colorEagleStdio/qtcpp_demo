#include <iostream>
#include <cassert>
#include "findfoo.h"
using namespace std;

int main()
{
	FFHANDLE h = ff_init_task("foobar");
	assert(h);
	cout << "Task string:" << ff_get_task(h) << endl;
	cout << "Input String:";
	std::string strRaw;
	cin >> strRaw;
	cout << ff_find(h,strRaw.c_str());
	//Delete
	ff_fini_task(h);
	h = nullptr;
	return 0;
}
