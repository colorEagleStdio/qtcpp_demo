#ifndef CHESSPI_H
#define CHESSPI_H
#include <vector>
#include <string>
/**
 * @brief The chess_node struct 棋盘状态节点
 */
struct chess_node{
	/**
	 * @brief coords 棋盘坐标，
	 * 每个字节一个棋子，X（高4比特）Y（低4比特），X1-9,Y1-10
	 * 顺序：0=红方， 1=黑方
	 * 0                         15 16                       31
	 * 帅士士相相马马车车炮炮兵兵兵兵兵  將仕仕象象馬馬車車砲砲卒卒卒卒卒
	 */
	unsigned char coords[32]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	/*!
	 * \brief alive 存活标记，每个1比特.顺序：0=红方， 1=黑方
	 * 0                        15 16                        31
	 * 帅士士相相马马车车炮炮兵兵兵兵兵  將仕仕象象馬馬車車砲砲卒卒卒卒卒
	 */
	unsigned int alive = 0;
	//遍历
	float weight = 1;
	unsigned int parent = 0;
	//unsigned int leaves = 0;
	float jump_cost[2]{0,0};//跳转损失
	char side = 0;
	char depth = 0;
	char killed = 0;
};
extern int max_depth;
extern int max_nodes;

//走位和显示
//-------------------------------------------
std::vector<chess_node> expand_node(const chess_node & root, const int side,bool onlykill=false);
//移动棋子
bool move_node(const chess_node & root, chess_node * new_node,const int oldx, const int oldy,const int newx, const int newy,const int side);
//转换松散坐标为棋子的压缩格式坐标
bool build_node(const int coordx[/*32*/], const int coordy[/*32*/],const int alive[/*32*/],const int idx, const int newx, const int newy,const int side,int map_coords[/*11*/][10],chess_node * node,bool onlykill=false);
bool build_node(const int coordx[/*32*/], const int coordy[/*32*/],const int alive[/*32*/],const int side,chess_node * node);
//打印棋盘
void print_node(const chess_node & node,const chess_node & old_node);
//X镜像反转坐标
void mirror_coordx(const unsigned char cod1[/*32*/],unsigned char cod2[/*32*/]);
//计算棋盘哈希
std::vector<size_t> node2hash(const std::vector<chess_node> & nodes);
size_t node2hash(const unsigned char cod[/*32*/], const unsigned int alive);

//AI
//-------------------------------------------
//计算走位代价
float calc_cost(const int coordx[/*32*/], const int coordy[/*32*/],const int alive[/*32*/],const int killed,const int idx);
//建立决策树
std::vector<chess_node> build_tree(const chess_node & root, const int side,const std::vector<chess_node> & history);
//评估并返回走位
size_t judge_tree(std::vector<chess_node> & tree);

#endif // CHESSPI_H
