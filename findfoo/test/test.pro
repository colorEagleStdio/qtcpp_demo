TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp
DESTDIR = $$OUT_PWD/../bin
INCLUDEPATH += ../findfoo

if (*static*){
    message($$QMAKESPEC);
    LIBS += -lfindfoo.dll -L$$DESTDIR
}
else{
    LIBS += -lfindfoo -L$$DESTDIR
}
