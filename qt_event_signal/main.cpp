#include <QCoreApplication>
#include <QThread>
#include "testevent.h"
#include "testobj.h"
int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	clock_t clk_start = clock();
	printf("StartClk = %d\n",clk_start);
	QThread::msleep(100);

	QThread * thread1 = new QThread;
	QThread * thread2 = new QThread;

	TestObj * obj1 = new TestObj;
	TestObj * obj2 = new TestObj;

	obj1->setObjectName("OBJ1");
	obj2->setObjectName("OBJ2");

	obj1->setBuddy(obj2);

	obj1->connect(obj1,&TestObj::test_sig1,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig2,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig3,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig4,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig5,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig6,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig7,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig8,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig9,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig10,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig11,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig12,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig13,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig14,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig15,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig16,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig17,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig18,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig19,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig20,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig21,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig22,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig23,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig24,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig25,obj2,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj1,&TestObj::test_sig26,obj2,&TestObj::test_slot16,Qt::QueuedConnection);

	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot1,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot2,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot3,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot4,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot5,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot6,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot7,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot8,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot9,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot10,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot11,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot12,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot13,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot14,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot15,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot16,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot17,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot18,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot19,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot20,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot21,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot22,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot23,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot24,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot25,Qt::QueuedConnection);
	obj1->connect(obj2,&TestObj::test_sig16,obj1,&TestObj::test_slot26,Qt::QueuedConnection);

	obj1->moveToThread(thread1);
	obj2->moveToThread(thread2);

	thread1->start();
	thread2->start();

	printf("Test signals & slots, Events...\n");
	QCoreApplication::processEvents();

	a.connect (obj1,&TestObj::sig_finished,[=]()->void{
		QThread::msleep(1000);
		QCoreApplication::postEvent(obj1,new QEvent(TestMsg::startEvt()));
	});
	a.connect (obj2,&TestObj::sig_finished,[=]()->void{
		QThread::msleep(1000);
		QCoreApplication::postEvent(obj2,new QEvent(TestMsg::startEvt()));
	});

	a.connect (obj1,&TestObj::evt_finished,[=]()->void{
		QThread::msleep(1000);
		QCoreApplication::postEvent(obj1,new QEvent(TestMsg::quitEvt()));
	});
	a.connect (obj2,&TestObj::evt_finished,[=]()->void{
		QThread::msleep(1000);
		QCoreApplication::postEvent(obj2,new QEvent(TestMsg::quitEvt()));
	});

	QThread::msleep(1000);
	QCoreApplication::processEvents();

	QCoreApplication::postEvent(obj1,new QEvent(TestMsg::startSig()));
	QCoreApplication::postEvent(obj2,new QEvent(TestMsg::startSig()));

	a.processEvents();

	thread1->wait();
	thread2->wait();

	QThread::msleep(2000);

	obj1->runDirectCall();

	printf("Finished.\n");

	thread1->deleteLater();
	thread2->deleteLater();
	obj1->deleteLater();
	obj2->deleteLater();
	QThread::msleep(1000);
	QCoreApplication::processEvents();

	return 0;
}
