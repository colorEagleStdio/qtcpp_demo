#include <stdio.h>
#include <memory.h>
#include "waveform.h"
//0,1对应的波形
SPTYPE wav_spread[2][WAVSIZE][2];
//0,1单个符号的表达。
static SPTYPE wav[2][MODRATE_N][2];
//用于相关的共轭
float wav_xorr[2][WAVSIZE][2];
void init_wavform(void)
{
	for (int i=0;i<MODRATE_N;++i)
	{
		wav[0][i][0] = C_AMP ;
		wav[0][i][1] = C_AMP ;
		wav[1][i][0] = -C_AMP ;
		wav[1][i][1] = -C_AMP ;
	}
	//扩频
	char reg [21] = {0,1,1,0,0,1,0,1,1,0,0,1,0,1,1,0,0,1,0,1,1};
	//seq[0]是 0的符号，1是1的符号。每个符号中的0,1又对应了wav的IQ路
	char seq[2][SPREAD_RAT];
	memset(seq,0,sizeof(seq));
	int sw = 0;
	//产生0,1的波形
	fprintf (stderr,"0=");

	for (int i=0;i<SPREAD_RAT;++i)
	{
		reg[sw % 21] = reg[(sw + 3) % 21] ^  reg[(sw+20) % 21];
		seq[0][i] = reg[sw % 21];
		fprintf (stderr,"%d",seq[0][i]);
		--sw;
		if (sw < 0)
			sw = 20;
		for (int j=0;j<MODRATE_N;++j)
		{
			wav_spread[0][i*MODRATE_N+j][0] = wav[seq[0][i]][j][0];
			wav_spread[0][i*MODRATE_N+j][1] = wav[seq[0][i]][j][1];
			wav_xorr[0][i*MODRATE_N+j][0] = wav[seq[0][i]][j][0]*1.0/C_AMP;
			wav_xorr[0][i*MODRATE_N+j][1] = -wav[seq[0][i]][j][1]*1.0/C_AMP;
		}
	}
	for (int i=0;i<377;++i)
	{
		reg[sw % 21] = reg[(sw + 3) % 21] ^  reg[(sw+20) % 21];
		--sw;
		if (sw < 0)
			sw = 20;
	}
	fprintf (stderr,"\n1=");
	for (int i=0;i<SPREAD_RAT;++i)
	{
		reg[sw % 21] = reg[(sw + 3) % 21] ^  reg[(sw+20) % 21];
		seq[1][i] = reg[sw % 21];
		fprintf (stderr,"%d",seq[1][i]);
		--sw;
		if (sw < 0)
			sw = 20;
		for (int j=0;j<MODRATE_N;++j)
		{
			wav_spread[1][i*MODRATE_N+j][0] = wav[seq[1][i]][j][0];
			wav_spread[1][i*MODRATE_N+j][1] = wav[seq[1][i]][j][1];
			wav_xorr[1][i*MODRATE_N+j][0] = wav[seq[1][i]][j][0]*1.0/C_AMP;
			wav_xorr[1][i*MODRATE_N+j][1] = -wav[seq[1][i]][j][1]*1.0/C_AMP;
		}
	}
	fprintf(stderr, "\nData inited.\n");
}
