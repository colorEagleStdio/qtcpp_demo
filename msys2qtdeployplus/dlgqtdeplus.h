#ifndef DLGQTDEPLUS_H
#define DLGQTDEPLUS_H

#include <QDialog>
#include <QStandardItemModel>
#include <QFileInfoList>
QT_BEGIN_NAMESPACE
namespace Ui {
class DlgQtDeplus;
}
QT_END_NAMESPACE

class DlgQtDeplus : public QDialog
{
	Q_OBJECT

public:
	DlgQtDeplus(QWidget *parent = nullptr);
	~DlgQtDeplus();

private slots:
	void on_toolButton_targetFolder_clicked();
	void on_toolButton_extra_folders_clicked();
	void on_toolButton_path_front_clicked();
	void on_toolButton_path_back_clicked();
	void on_pushButton_run_clicked();
	void on_toolButton_msys2_clicked();

private:
	Ui::DlgQtDeplus *ui;
	QStandardItemModel * m_pMsgMod {nullptr};
	bool running = false;
	bool stopcmd = false;
protected:
	void loadIni();
	void saveIni();
	void scrollPlainTextToBtm();
protected:
	int run_deployqt();
	int run_ldd();
	void enumAllExes(QString folder,QFileInfoList * pLst);
public slots:
	void showMsg(QString);
};
#endif // DLGQTDEPLUS_H
