#include <QCoreApplication>
#include <QList>
#include "listenthread.h"
#include <fcgi_stdio.h>

using namespace std;


const int thread_count = 4;

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	//Load fcgi libraries
	FCGX_Init();
	QList<listenThread *> threadpool;
	for (int i=0;i<thread_count;++i)
		threadpool.push_back(new listenThread(&a));

	foreach (listenThread * t, threadpool)
	{
		t->start();
	}
	int alives = 0;
	do
	{
		alives = 0;
		foreach (listenThread * t, threadpool)
		{
			if (t->isRunning())
			{
				++alives;
				t->wait(200);
			}
			else
				QThread::msleep(200);
			a.processEvents();
		}
	}while (alives>0);

	a.quit();
	return 0;
}
