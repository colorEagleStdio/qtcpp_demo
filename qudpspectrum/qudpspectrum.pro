QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    dlgtest.cpp \
    spectrum/qtgui/agc_options.cpp \
    spectrum/qtgui/bookmarks.cpp \
    spectrum/qtgui/bookmarkstablemodel.cpp \
    spectrum/qtgui/bookmarkstaglist.cpp \
    spectrum/qtgui/demod_options.cpp \
    spectrum/qtgui/dockrxopt.cpp \
    spectrum/qtgui/freqctrl.cpp \
    spectrum/qtgui/meter.cpp \
    spectrum/qtgui/nb_options.cpp \
    spectrum/qtgui/plotter.cpp \
    spectrum/specwidget.cpp \
    udp/cudpreciever.cpp

HEADERS += \
    dlgtest.h \
    spectrum/qtgui/agc_options.h \
    spectrum/qtgui/bookmarks.h \
    spectrum/qtgui/bookmarkstablemodel.h \
    spectrum/qtgui/bookmarkstaglist.h \
    spectrum/qtgui/demod_options.h \
    spectrum/qtgui/dockrxopt.h \
    spectrum/qtgui/freqctrl.h \
    spectrum/qtgui/meter.h \
    spectrum/qtgui/nb_options.h \
    spectrum/qtgui/plotter.h \
    spectrum/specwidget.h \
    udp/cudpreciever.h

FORMS += \
    dlgtest.ui \
    spectrum/qtgui/agc_options.ui \
    spectrum/qtgui/demod_options.ui \
    spectrum/qtgui/dockrxopt.ui \
    spectrum/qtgui/nb_options.ui \
    spectrum/specwidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32{
    mkoptions = $$find(QMAKESPEC, "vc")
    count(mkoptions, 1){
    INCLUDEPATH +="$$PWD/3rdlibs/win32/fftw"
    contains(QT_ARCH, i386) {
	LIBS+=-L"$$PWD/3rdlibs/win32/fftw/x86" -llibfftw3-3
    } else {
	LIBS+=-L"$$PWD/3rdlibs/win32/fftw/x64" -llibfftw3-3
    }
    } else: LIBS+=-lfftw3
}else: LIBS+=-lfftw3
