#ifndef DLGSTP_H
#define DLGSTP_H

#include <QDialog>
#include <QGeoPositionInfoSource>
#include <QStandardItemModel>
#include <QUdpSocket>
#include <QTcpSocket>
#include <QSensor>
#include <functional>
QT_BEGIN_NAMESPACE
namespace Ui { class DlgSTP; }
QT_END_NAMESPACE

class DlgSTP : public QDialog
{
	Q_OBJECT
public:
	DlgSTP(QWidget *parent = nullptr);
	~DlgSTP();
	void EnumSensors();
	void openGPS();
	void loadSettings();
	void saveSettings();
protected:
	void timerEvent(QTimerEvent * evt) override;
private:
	Ui::DlgSTP *ui;
	QStandardItemModel * m_pMsgMod = 0;
	int m_nTimer = -1;
	unsigned long long m_clk = 0;
	unsigned long long m_nTotalSent = 0;
protected:
	//Sensors
	QList<std::function<void (void)> > m_sensorUpdaters;
protected:
	//GPS
	QGeoPositionInfoSource *m_pos_source = 0;
	QDateTime m_lastGpsTime;
protected:
	//Net Send
	QUdpSocket * m_psock = 0;
	QTcpSocket * m_pstream = 0;
	QStringList m_listInfo;
private slots:
	void on_pushButton_left_clicked();
	void on_pushButton_right_clicked();
	void on_horizontalSlider_gps_valueChanged(int value);
	void on_tabWidget_currentChanged(int index);
	void on_checkBox_udp_clicked(bool checked);
	void on_checkBox_tcp_clicked(bool checked);
};
#endif // DLGSTP_H
