﻿#ifndef DLGTEST_H
#define DLGTEST_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class DlgTest; }
QT_END_NAMESPACE

class DlgTest : public QDialog
{
	Q_OBJECT

public:
	DlgTest(QWidget *parent = nullptr);
	~DlgTest();
protected:
	void timerEvent(QTimerEvent * e) override;
private:
	Ui::DlgTest *ui;
	int m_nTid = -1;
};
#endif // DLGTEST_H
