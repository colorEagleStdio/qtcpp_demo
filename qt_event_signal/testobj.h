#ifndef TESTOBJ_H
#define TESTOBJ_H

#include <QObject>
#include <QEvent>
#include "testevent.h"
class TestObj : public QObject
{
	Q_OBJECT
public:
	explicit TestObj(QObject *parent = nullptr);
	void setBuddy(TestObj * buddy) {
		m_buddy = buddy;
		buddy->m_buddy = this;
	}
	void runDirectCall();
public slots:
	void test_slot1(QEvent * evt){}
	void test_slot2(QEvent * evt){}
	void test_slot3(QEvent * evt){}
	void test_slot4(QEvent * evt){}
	void test_slot5(QEvent * evt){}
	void test_slot6(QEvent * evt){}
	void test_slot7(QEvent * evt){}
	void test_slot8(QEvent * evt){}
	void test_slot9(QEvent * evt){}
	void test_slot10(QEvent * evt){}
	void test_slot11(QEvent * evt){}
	void test_slot12(QEvent * evt){}
	void test_slot13(QEvent * evt){}
	void test_slot14(QEvent * evt){}
	void test_slot15(QEvent * evt){}
	void test_slot16(QEvent * evt);
	void test_slot17(QEvent * evt){}
	void test_slot18(QEvent * evt){}
	void test_slot19(QEvent * evt){}
	void test_slot20(QEvent * evt){}
	void test_slot21(QEvent * evt){}
	void test_slot22(QEvent * evt){}
	void test_slot23(QEvent * evt){}
	void test_slot24(QEvent * evt){}
	void test_slot25(QEvent * evt){}
	void test_slot26(QEvent * evt){}
signals:
	void test_sig1(QEvent * evt);
	void test_sig2(QEvent * evt);
	void test_sig3(QEvent * evt);
	void test_sig4(QEvent * evt);
	void test_sig5(QEvent * evt);
	void test_sig6(QEvent * evt);
	void test_sig7(QEvent * evt);
	void test_sig8(QEvent * evt);
	void test_sig9(QEvent * evt);
	void test_sig10(QEvent * evt);
	void test_sig11(QEvent * evt);
	void test_sig12(QEvent * evt);
	void test_sig13(QEvent * evt);
	void test_sig14(QEvent * evt);
	void test_sig15(QEvent * evt);
	void test_sig16(QEvent * evt);
	void test_sig17(QEvent * evt);
	void test_sig18(QEvent * evt);
	void test_sig19(QEvent * evt);
	void test_sig20(QEvent * evt);
	void test_sig21(QEvent * evt);
	void test_sig22(QEvent * evt);
	void test_sig23(QEvent * evt);
	void test_sig24(QEvent * evt);
	void test_sig25(QEvent * evt);
	void test_sig26(QEvent * evt);

	void evt_finished();
	void sig_finished();
protected:
	void customEvent(QEvent *) override;
private:
	clock_t m_nFirstClkSig = -1;
	quint32 m_nCount_Sigs = 0;

	clock_t m_nFirstClkEvt = -1;
	quint32 m_nCount_Evts = 0;

	clock_t m_nFirstClkDir = -1;
	quint32 m_nCount_Dir = 0;
private:
	TestObj * m_buddy = nullptr;
private:
	void run_signal();
	void run_event();
private:
	void direct_call(QEvent * evt);
};

#endif // TESTOBJ_H
